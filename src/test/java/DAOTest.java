
import com.code.DAO.AppDataDAO;
import com.code.Model.UserData;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import junit.framework.Assert;
import org.junit.Before;
import org.junit.Test;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.mock;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Dominik
* @version 1.0
 */
public class DAOTest {
    
    private AppDataDAO dataDAO ;
    
    @Before
    public void injectMockEntityManager(){
        dataDAO = new AppDataDAO();
       EntityManager entityManager =  mock(EntityManager.class);
       dataDAO.entityManager = entityManager;
    }
    
    @Test
    public void testGetAllData(){
        List<UserData> dataList = new ArrayList<>();
        dataList = createUserDataList();
        TypedQuery mockQuery = getQueryThatReturnsList(dataList);
        when(dataDAO.entityManager.createNamedQuery(anyString(),any()))
                .thenReturn(mockQuery);
        Assert.assertEquals(dataList, dataDAO.getAllUserData());    
        verify(dataDAO.entityManager).createNamedQuery(eq("allData"),eq(UserData.class));
    }
    
    public List<UserData> createUserDataList(){
        List<UserData> userData = new ArrayList<UserData>();
        UserData data = new UserData();
        data.setFilename("test");
        data.setScreenDpi(101);
        data.setScreenWidth(200);
        data.setScreenHeight(300);
        data.setId(0);
        userData.add(data);
        return userData;
        
    }
    
     private TypedQuery<UserData> getQueryThatReturnsList(List list) {  
  TypedQuery<UserData> mockQuery = mock(TypedQuery.class);  
  when(mockQuery.getResultList()).thenReturn(list);  
  return mockQuery;  
 }
}
