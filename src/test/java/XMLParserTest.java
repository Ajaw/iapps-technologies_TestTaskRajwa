import com.code.Model.UserData;
import com.code.Utils.XMLParser;
import com.code.Utils.XMLValidator;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import javax.xml.stream.XMLStreamException;
import java.io.File;

/**
 * Created by Dominik on 16.06.2017.
 */
public class XMLParserTest {

    private String wrongPath;
    private String correctPath;

    @Before
    public void setupTest() {
        correctPath  = getPathForResource("correct.xml");
        wrongPath = getPathForResource("wrong.xml");
    }

    @Test
    public void xmlValidationTest(){

        XMLValidator validator = new XMLValidator();

        boolean result = validator.vaildateXmlFile(correctPath);
        Assert.assertEquals(result,true);

        result = validator.vaildateXmlFile(wrongPath);
        Assert.assertEquals(result,false);

    }

    @Test
    public void doesXMLParsingWork(){
        XMLParser parser = new XMLParser();

        try {
           UserData userData = parser.extractDataFromXmlFile(correctPath);

           Assert.assertEquals(userData.getScreenDpi(),Integer.valueOf(160));
           Assert.assertEquals(userData.getScreenWidth(),Integer.valueOf(1280));
           Assert.assertEquals(userData.getScreenHeight(),Integer.valueOf(752));
           Assert.assertEquals(userData.getNewspaperName(),"abb");
            System.out.println("end test");
        } catch (XMLStreamException e) {
            Assert.fail();
        }
        catch (Exception ex){
            Assert.fail();
        }

    }

    private String  getPathForResource(String resourceName){
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource(resourceName).getFile());
        String path = file.getPath();
        return  path;
    }



}
