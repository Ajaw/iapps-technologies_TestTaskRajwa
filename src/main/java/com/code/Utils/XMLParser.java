package com.code.Utils;

import com.code.Model.UserData;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.transform.stream.StreamSource;
import java.io.File;

/**
 * Created by Dominik on 16.06.2017.
 */

public class XMLParser {

    public UserData extractDataFromXmlFile(String xmlFilePath) throws XMLStreamException {
        XMLInputFactory inputFactory = XMLInputFactory.newInstance();
        XMLStreamReader reader = inputFactory.createXMLStreamReader(new StreamSource(new File(xmlFilePath)));
        try {
            return readDocument(reader);
        }
        finally {
            if (reader!=null){
                reader.close();
            }
        }

    }

    private UserData readDocument (XMLStreamReader reader) throws XMLStreamException {
        while (reader.hasNext()) {
            int eventType = reader.next();
            switch (eventType) {
                case XMLStreamReader.START_ELEMENT:
                    String elemnetName = reader.getLocalName();
                    if (elemnetName.equals("epaperRequest")) {
                        return readDeviceInfo(reader);
                    }
                case XMLStreamReader.END_ELEMENT:
                    break;
            }
        }
        throw new XMLStreamException("Premature end of file");
    }

    private UserData readDeviceInfo(XMLStreamReader reader) throws XMLStreamException {
        while (reader.hasNext()){
            int eventType = reader.next();
            switch (eventType){
                case XMLStreamReader.START_ELEMENT:
                    String elementName = reader.getLocalName();
                    if (elementName.equals("deviceInfo")){
                        return readUserData(reader);
                    }
                case XMLStreamReader.END_ELEMENT:
                    break;
            }

        }
        throw new XMLStreamException("Premature end of file");
    }

    private UserData readUserData(XMLStreamReader reader) throws XMLStreamException {
        UserData data = new UserData();
        while (reader.hasNext()){
            int eventType = reader.next();
            switch (eventType){
                case XMLStreamReader.START_ELEMENT:
                    String elementName = reader.getLocalName();
                    if (elementName.equals("screenInfo")){
                        readScreenInfoData(reader,data);
                    }
                    else if (elementName.equals("osInfo")) {
                        readOsInfoData(reader);
                    }
                    else if (elementName.equals("appInfo")) {
                        data.setNewspaperName(readAppInfoData(reader));
                    }
                    break;
                case XMLStreamReader.END_ELEMENT:
                    return data;
            }

        }
        throw new XMLStreamException("Premature end of file");
    }


    private UserData readScreenInfoData(XMLStreamReader reader, UserData data) throws XMLStreamException {
        String elementName = reader.getLocalName();
        if (elementName.equals("screenInfo")){
            String dpi = reader.getAttributeValue(null, "dpi");
            data.setScreenDpi(Integer.parseInt(dpi));
            String width = reader.getAttributeValue(null, "width");
            data.setScreenWidth(Integer.parseInt(width));
            String height = reader.getAttributeValue(null, "height");
            data.setScreenHeight(Integer.parseInt(height));
        }
        reader.next();
        return data;

    }


    private void readOsInfoData(XMLStreamReader reader) throws XMLStreamException {
        while (reader.hasNext()){
            int eventType = reader.next();
            switch (eventType){
                case XMLStreamReader.START_ELEMENT:
                    String elementName = reader.getLocalName();

                case XMLStreamReader.END_ELEMENT:
                    return;
            }
        }
        throw new XMLStreamException("Premature end of file");
    }

    private String readAppInfoData(XMLStreamReader reader) throws XMLStreamException {
        while (reader.hasNext()){
            int eventType = reader.next();
            switch (eventType){
                case XMLStreamReader.START_ELEMENT:
                    String elementName = reader.getLocalName();
                    if (elementName.equals("newspaperName")){
                        return reader.getElementText();
                    }
                case XMLStreamReader.END_ELEMENT:
                    break;
            }
        }
        throw new XMLStreamException("Premature end of file");
    }

}

