package com.code.Utils;

import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.File;
import java.io.IOException;

/**
 * Created by Dominik on 16.06.2017.
 */
public class XMLValidator {

    private String schemaFileName = "schema.xsd";

    public boolean vaildateXmlFile(String xmlFilePath){
        Source xmlFile = new StreamSource(new File(xmlFilePath));

        SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);

        try {
            Schema schema = schemaFactory.newSchema(getSchemaFile());
            Validator validator = schema.newValidator();
            validator.validate(xmlFile);
        }
        catch (SAXException ex){
            return  false;
        } catch (IOException e) {
            return  false;
        }

        return true;


    }



    private File getSchemaFile(){
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource(schemaFileName).getFile());
        return  file;
    }
}
