package com.code.DAO;

import com.code.Model.UserData;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

/**
 * Created by Dominik on 14.06.2017.
 */
@Stateless
public class AppDataDAO {

    @PersistenceContext(unitName = "NewPersistanceUnit")
    public EntityManager entityManager;

    public List<UserData> getAllUserData(){

        TypedQuery<UserData> query = entityManager.createNamedQuery("allData",UserData.class);
        return  query.getResultList();
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRED)
    public UserData saveData(UserData data){
        entityManager.persist(data);
        entityManager.flush();
        return data;
    }

}
