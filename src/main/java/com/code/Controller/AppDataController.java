package com.code.Controller;

import com.code.DAO.AppDataDAO;
import com.code.Model.UserData;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.SessionScoped;
import java.io.Serializable;
import java.util.List;


/**
 * Created by Dominik on 14.06.2017.
 */

@javax.faces.bean.ManagedBean(name = "dataCon")
@SessionScoped
public class AppDataController implements Serializable {


    @EJB
    AppDataDAO myDao;

    private List<UserData> userData;

    private void getAllData(){
        System.out.println("acess data");
         userData=  myDao.getAllUserData();
    }

    @PostConstruct
    public void init(){
        getAllData();
    }

    public List<UserData> getUserData() {
        System.out.println("refresh");
        getAllData();
        return userData;
    }

    public void setUserData(List<UserData> userData) {
        this.userData = userData;
    }


}
