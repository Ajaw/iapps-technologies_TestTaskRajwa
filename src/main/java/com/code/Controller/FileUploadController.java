package com.code.Controller;

import com.code.DAO.AppDataDAO;
import com.code.Model.UserData;
import com.code.Utils.XMLParser;
import com.code.Utils.XMLValidator;
import org.primefaces.model.UploadedFile;


import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.xml.stream.XMLStreamException;
import java.io.*;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@ManagedBean(name = "fileUpload")
@SessionScoped
public class FileUploadController implements Serializable {


    private UploadedFile file;

    public UploadedFile getFile() {
        return file;
    }

    @EJB
    AppDataDAO myDao;

    public void setUserData(UserData data){

        myDao.saveData(data);
    }

    public List<UserData> getAllData(){
        return myDao.getAllUserData();
    }

    public void setFile(UploadedFile file) {
        this.file = file;
    }

    public String uploadAction(){
        FacesContext context = FacesContext.getCurrentInstance();
        try {

            System.out.println("test");
            System.out.println("Uploaded File Name Is :: "+file.getFileName()+" :: Uploaded File Size :: "+file.getSize());


            Path folder = Paths.get("");
            String fileName = file.getFileName();
            String extension = "";

            int i = fileName.lastIndexOf('.');

            if (i > 0) {
                extension = fileName.substring(i+1);
                fileName = fileName.substring(0,i);
            }
            if (!(extension.equals("xml"))){

                context.addMessage(null,new FacesMessage("Wrong File Format"));
            }
            else {
                Path filePath = Files.createTempFile(folder, fileName + "-", "." + extension);
                try (InputStream input = file.getInputstream()) {
                    Files.copy(input, filePath, StandardCopyOption.REPLACE_EXISTING);
                }

                extractDataFromFile(filePath.toString());
            }
        }
        catch (Exception ex){
            context.addMessage(null,new FacesMessage("Error"));
        }


        return "";
    }

    private void extractDataFromFile(String xmlFilePath){
        XMLValidator validator = new XMLValidator();

        if (validator.vaildateXmlFile(xmlFilePath)){
            try {
                FacesContext context= FacesContext.getCurrentInstance();
                XMLParser parser = new XMLParser();
              UserData data = parser.extractDataFromXmlFile(xmlFilePath);
                Date date = new Date();
              data.setUploadTime(new Timestamp(date.getTime()));
              data.setFilename(file.getFileName());
              setUserData(data);
                context.addMessage(null,new FacesMessage("Upload Succesful"));
            } catch (XMLStreamException e) {
                e.printStackTrace();

            }
        }
        else {
            FacesContext context= FacesContext.getCurrentInstance();
            context.addMessage(null,new FacesMessage("Incorrect XML Structure"));
        }
    }




}
