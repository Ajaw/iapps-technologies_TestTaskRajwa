package com.code.Model;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

/**
 * Created by Dominik on 14.06.2017.
 */
@Entity
@Table(name = "AppUserData")
@NamedQueries({
        @NamedQuery(name = "allData", query = "SELECT u from UserData u")}
)
public class UserData implements Serializable {
    private int id;
    private Integer screenWidth;
    private Integer screenHeight;
    private Integer screenDpi;
    private String newspaperName;
    private Timestamp uploadTime;
    private String filename;

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "screen_width")
    public Integer getScreenWidth() {
        return screenWidth;
    }

    public void setScreenWidth(Integer screenWidth) {
        this.screenWidth = screenWidth;
    }

    @Basic
    @Column(name = "screen_height")
    public Integer getScreenHeight() {
        return screenHeight;
    }

    public void setScreenHeight(Integer screenHeight) {
        this.screenHeight = screenHeight;
    }

    @Basic
    @Column(name = "screen_dpi")
    public Integer getScreenDpi() {
        return screenDpi;
    }

    public void setScreenDpi(Integer screenDpi) {
        this.screenDpi = screenDpi;
    }

    @Basic
    @Column(name = "newspaper_name")
    public String getNewspaperName() {
        return newspaperName;
    }

    public void setNewspaperName(String newspaperName) {
        this.newspaperName = newspaperName;
    }

    @Basic
    @Column(name = "upload_time")
    public Timestamp getUploadTime() {
        return uploadTime;
    }

    public void setUploadTime(Timestamp uploadTime) {
        this.uploadTime = uploadTime;
    }

    @Basic
    @Column(name = "filename")
    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }
    
}
