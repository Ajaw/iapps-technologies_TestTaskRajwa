CREATE TABLE `AppUserData` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `screen_width` int(11) DEFAULT NULL,
  `screen_height` int(11) DEFAULT NULL,
  `screen_dpi` int(11) DEFAULT NULL,
  `newspaper_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `upload_time` datetime DEFAULT NULL,
  `filename` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `table_name_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci';